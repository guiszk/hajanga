#!/usr/bin/env python3.7
import random

def gw(x):
    return str(random.choice(list(open(x))).strip())

def vouns():
    if(random.randint(0, 100)%2 == 0):
        return(gw("adjectives.txt") + " "  + gw("nouns_old.txt"))
    else:
        return(gw("vouns.txt"))

print(gw("names.txt"), gw("verbs.txt"), gw("vouns.txt"), gw("connectors.txt"), vouns() + ".")

